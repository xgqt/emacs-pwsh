;;; pwsh-imenu.el --- PowerShell scripting support -*- lexical-binding: t; -*-


;; This file is part of emacs-pwsh - PowerShell support for Emacs.
;; Copyright (c) 2022, Maciej Barć <xgqt@riseup.net>
;; Licensed under the GNU GPL v3 License
;; SPDX-License-Identifier: GPL-3.0-or-later

;; emacs-pwsh is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; emacs-pwsh is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with emacs-pwsh.  If not, see <https://www.gnu.org/licenses/>.



;;; Commentary:


;; PWSH Imenu support.



;;; Code:


(require 'pwsh-core)


(defvar pwsh-imenu-expression
  `(("Functions" ,(concat "function " pwsh-function-names-regex) 2)
    ("Filters" ,(concat "filter " pwsh-function-names-regex) 2)
    ("Top variables"
     , (concat "^\\(" pwsh-object-types-regexp "\\)?\\("
               pwsh-variables-regexp "\\)\\s-*=")
     2))
  "List of regexps matching important expressions, for speebar & imenu.")


(defun pwsh-setup-imenu ()
  "Install `pwsh-imenu-expression'."
  (when (require 'imenu nil t)
    ;; imenu doc says these are buffer-local by default
    (setq imenu-generic-expression pwsh-imenu-expression)
    (setq imenu-case-fold-search nil)
    (imenu-add-menubar-index)))


(provide 'pwsh-imenu)



;;; pwsh-imenu.el ends here
