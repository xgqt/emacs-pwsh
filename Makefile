EMACS       := emacs
FIND        := find

EMACFLAGS   := --batch -q --no-site-file
EMACSCMD     = $(EMACS) $(EMACFLAGS)

PWD         ?= $(shell pwd)


.PHONY: all
all: clean compile

.PHONY: clean
clean:
	$(FIND) $(PWD) -iname "*.elc" -delete

%.elc:
	$(EMACSCMD) -L $(PWD) --eval "(byte-compile-file \"$(*).el\" 0)"

.PHONY: compile
compile: pwsh-core.elc pwsh-eldoc.elc pwsh-imenu.elc pwsh.elc

.PHONY: install
install: compile
	$(EMACSCMD) \
		--eval "(require 'package)" --eval "(package-install-file \"$(PWD)\")"
