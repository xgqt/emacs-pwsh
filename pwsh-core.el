;;; pwsh-core.el --- PowerShell scripting support -*- lexical-binding: t; -*-


;; This file is part of emacs-pwsh - PowerShell support for Emacs.
;; Copyright (c) 2022, Maciej Barć <xgqt@riseup.net>
;; Licensed under the GNU GPL v3 License
;; SPDX-License-Identifier: GPL-3.0-or-later

;; emacs-pwsh is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; emacs-pwsh is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with emacs-pwsh.  If not, see <https://www.gnu.org/licenses/>.



;;; Commentary:


;; PowerShell scripting support.



;;; Code:


(eval-when-compile (require 'thingatpt))


(defgroup pwsh nil
  "Customization of PowerShell mode."
  :link '(custom-group-link :tag "Font Lock Faces group" font-lock-faces)
  :group 'languages)


(defcustom pwsh-indent 4
  "Amount of horizontal space to indent.
After, for instance, an opening brace"
  :type 'integer
  :group 'pwsh)

(defcustom pwsh-continuation-indent 2
  "Amount of horizontal space to indent a continuation line."
  :type 'integer
  :group 'pwsh)

(defcustom pwsh-continued-regexp  ".*\\(|[\\t ]*\\|`\\)$"
  "Regexp matching a continued line.
Ending either with an explicit backtick, or with a pipe."
  :type 'integer
  :group 'pwsh)


(defun pwsh-continuation-line-p ()
  "Return t is the current line is a continuation line.
The current line is a continued line when the previous line ends
with a backtick or a pipe"
  (interactive)
  (save-excursion
    (forward-line -1)
    (looking-at pwsh-continued-regexp)))

;; Rick added significant complexity to Frédéric's original version
(defun pwsh-indent-line-amount ()
  "Return the column to which the current line ought to be indented."
  (interactive)
  (save-excursion
    (beginning-of-line)
    (if (pwsh-continuation-line-p)
        ;; on a continuation line (i.e. prior line ends with backtick
        ;; or pipe), indent relative to the continued line.
        (progn
          (while (and (not (bobp))(pwsh-continuation-line-p))
            (forward-line -1))
          (+ (current-indentation) pwsh-continuation-indent))
      ;; otherwise, indent relative to the block's opening char ([{
      ;; \\s- includes newline, which make the line right before closing paren not indented
      (let ((closing-paren (looking-at "[ \t]*\\s)"))
            new-indent
            block-open-line)
        (condition-case nil
            (progn
              (backward-up-list)   ;when at top level, throw to no-indent
              (setq block-open-line (line-number-at-pos))
              ;; We're in a block, calculate/return indent amount.
              (if (not (looking-at "\\s(\\s-*\\(#.*\\)?$"))
                  ;; code (not comments) follow the block open so
                  ;; vertically align the block with the code.
                  (if closing-paren
                      ;; closing indent = open
                      (setq new-indent (current-column))
                    ;; block indent = first line of code
                    (forward-char)
                    (skip-syntax-forward " ")
                    (setq new-indent (current-column)))
                ;; otherwise block open is at eol so indent is relative to
                ;; bol or another block open on the same line.
                (if closing-paren       ; this sets the default indent
                    (setq new-indent (current-indentation))
                  (setq new-indent (+ pwsh-indent (current-indentation))))
                ;; now see if the block is nested on the same line
                (when (condition-case nil
                          (progn
                            (backward-up-list)
                            (= block-open-line (line-number-at-pos)))
                        (scan-error nil))
                  (forward-char)
                  (skip-syntax-forward " ")
                  (if closing-paren
                      (setq new-indent (current-column))
                    (setq new-indent (+ pwsh-indent (current-column))))))
              new-indent)
          (scan-error ;; most likely, we are at the top-level
           0))))))

(defun pwsh-indent-line ()
  "Indent the current line of powershell mode.
Leave the point in place if it is inside the meat of the line"
  (interactive)
  (let ((savep (> (current-column) (current-indentation)))
        (amount (pwsh-indent-line-amount)))
    (if savep
        (save-excursion (indent-line-to amount))
      (indent-line-to amount))))

(defun pwsh-quote-selection (beg end)
  "Quotes the selection between BEG and END.
Quotes with single quotes and doubles embedded single quotes."
  (interactive `(,(region-beginning) ,(region-end)))
  (if (not mark-active)
      (error "Command requires a marked region"))
  (goto-char beg)
  (while (re-search-forward "'" end t)
    (replace-match "''")(setq end (1+ end)))
  (goto-char beg)
  (insert "'")
  (setq end (1+ end))
  (goto-char end)
  (insert "'"))

(defun pwsh-unquote-selection (beg end)
  "Unquotes the selected text between BEG and END.
Remove doubled single quotes as we go."
  (interactive `(,(region-beginning) ,(region-end)))
  (if (not mark-active)
      (error "Command requires a marked region"))
  (goto-char beg)
  (cond ((looking-at "'")
         (goto-char end)
         (when (looking-back "'" nil)
           (delete-char -1)
           (setq end (1- end))
           (goto-char beg)
           (delete-char 1)
           (setq end (1- end))
           (while (search-forward "'" end t)
             (delete-char -1)
             (forward-char)
             (setq end (1- end)))))
        ((looking-at "\"")
         (goto-char end)
         (when (looking-back "\"" nil)
           (delete-char -1)
           (setq end (1- end))
           (goto-char beg)
           (delete-char 1)
           (setq end (1- end))
           (while (search-forward "\"" end t)
             (delete-char -1)
             (forward-char)
             (setq end (1- end)))
           (while (search-forward "`" end t)
             (delete-char -1)
             (forward-char)
             (setq end (1- end)))))
        (t (error "Must select quoted text exactly"))))

(defun pwsh-escape-selection (beg end)
  "Escape variables between BEG and END.
Also extend existing escapes."
  (interactive `(,(region-beginning) ,(region-end)))
  (if (not mark-active)
      (error "Command requires a marked region"))
  (goto-char beg)
  (while (re-search-forward "`" end t)
    (replace-match "```")(setq end (+ end 2)))
  (goto-char beg)
  (while (re-search-forward "\\(?:\\=\\|[^`]\\)[$]" end t)
    (goto-char (car (cdr (match-data))))
    (backward-char)
    (insert "`")
    (forward-char)
    (setq end (1+ end))))

(defun pwsh-doublequote-selection (beg end)
  "Quotes the text between BEG and END with double quotes.
Embedded quotes are doubled."
  (interactive `(,(region-beginning) ,(region-end)))
  (if (not mark-active)
      (error "Command requires a marked region"))
  (goto-char beg)
  (while (re-search-forward "\"" end t)
    (replace-match "\"\"")(setq end (1+ end)))
  (goto-char beg)
  (while (re-search-forward "`'" end t)
    (replace-match "```")(setq end (+ 2 end)))
  (goto-char beg)
  (insert "\"")
  (setq end (1+ end))
  (goto-char end)
  (insert "\""))

(defun pwsh-dollarparen-selection (beg end)
  "Wraps the text between BEG and END with $().
The point is moved to the closing paren."
  (interactive `(,(region-beginning) ,(region-end)))
  (if (not mark-active)
      (error "Command requires a marked region"))
  (save-excursion
    (goto-char end)
    (insert ")")
    (goto-char beg)
    (insert "$("))
  (forward-char))

(defun pwsh-regexp-to-regex (beg end)
  "Turn the text between BEG and END into a regex.
The text is assumed to be `regexp-opt' output."
  (interactive `(,(region-beginning) ,(region-end)))
  (if (not mark-active)
      (error "Command requires a marked region"))
  (save-restriction
    (narrow-to-region beg end)
    (goto-char (point-min))
    (while (re-search-forward "\\\\(" nil t)
      (replace-match "("))
    (goto-char (point-min))
    (while (re-search-forward "\\\\)" nil t)
      (replace-match ")"))
    (goto-char (point-min))
    (while (re-search-forward "\\\\|" nil t)
      (replace-match "|"))))

;; Taken from About_Keywords
(defvar pwsh-keywords
  (concat "\\_<"
          (regexp-opt
           '("begin" "break" "catch" "class" "continue" "data" "define" "do" "default"
             "dynamicparam" "else" "elseif" "end" "enum" "exit" "filter" "finally"
             "for" "foreach" "from" "function" "hidden" "if" "in" "param" "process"
             "return" "static" "switch" "throw" "trap" "try" "until" "using" "var" "where" "while"
             ;; Questionable, specific to workflow sessions
             "inlinescript")
           t)
          "\\_>")
  "PowerShell keywords.")

;; Taken from About_Comparison_Operators and some questionable sources :-)
(defvar pwsh-operators
  (concat "\\_<"
          (regexp-opt
           '("-eq" "-ne" "-gt" "-ge" "-lt" "-le"
             ;; case sensitive versions
             "-ceq" "-cne" "-cgt" "-cge" "-clt" "-cle"
             ;; explicitly case insensitive
             "-ieq" "-ine" "-igt" "-ige" "-ilt" "-ile"
             "-band" "-bor" "-bxor" "-bnot"
             "-and" "-or" "-xor" "-not" "!"
             "-like" "-notlike" "-clike" "-cnotlike" "-ilike" "-inotlike"
             "-match" "-notmatch" "-cmatch" "-cnotmatch" "-imatch" "-inotmatch"
             "-contains" "-notcontains" "-ccontains" "-cnotcontains"
             "-icontains" "-inotcontains"
             "-replace" "-creplace" "-ireplace"
             "-is" "-isnot" "-as" "-f"
             "-in" "-cin" "-iin" "-notin" "-cnotin" "-inotin"
             "-split" "-csplit" "-isplit"
             "-join"
             "-shl" "-shr"
             ;; Questionable --> specific to certain contexts
             "-casesensitive" "-wildcard" "-regex" "-exact" ;specific to case
             "-begin" "-process" "-end" ;specific to scriptblock
             ) t)
          "\\_>")
  "PowerShell operators.")

(defvar pwsh-scope-names
  '("global"   "local"    "private"  "script"   )
  "Names of scopes in PowerShell mode.")

(defvar pwsh-variable-drive-names
  (append '("env" "function" "variable" "alias" "hklm" "hkcu" "wsman") pwsh-scope-names)
  "Names of scopes in PowerShell mode.")

(defconst pwsh-variables-regexp
  ;; There are 2 syntaxes detected: ${[scope:]name} and $[scope:]name
  ;; Match 0 is the entire variable name.
  ;; Match 1 is scope when the former syntax is found.
  ;; Match 2 is scope when the latter syntax is found.
  (concat
   "\\_<$\\(?:{\\(?:" (regexp-opt pwsh-variable-drive-names t)
   ":\\)?[^}]+}\\|"
   "\\(?:" (regexp-opt pwsh-variable-drive-names t)
   ":\\)?[a-zA-Z0-9_]+\\_>\\)")
  "Identifies legal powershell variable names.")

(defconst pwsh-function-names-regex
  ;; Syntax detected is [scope:]verb-noun
  ;; Match 0 is the entire name.
  ;; Match 1 is the scope if any.
  ;; Match 2 is the function name (which must exist)
  (concat
   "\\_<\\(?:" (regexp-opt pwsh-scope-names t) ":\\)?"
   "\\([A-Z][a-zA-Z0-9]*-[A-Z0-9][a-zA-Z0-9]*\\)\\_>")
  "Identifies legal function & filter names.")

(defconst pwsh-object-types-regexp
  ;; Syntax is \[name[.name]\] (where the escaped []s are literal)
  ;; Only Match 0 is returned.
  "\\[\\(?:[a-zA-Z_][a-zA-Z0-9]*\\)\\(?:\\.[a-zA-Z_][a-zA-Z0-9]*\\)*\\]"
  "Identifies object type references.  I.E. [object.data.type] syntax.")

(defconst pwsh-function-switch-names-regexp
  ;; Only Match 0 is returned.
  "\\_<-[a-zA-Z][a-zA-Z0-9]*\\_>"
  "Identifies function parameter names of the form -xxxx.")

;; Taken from Get-Variable on a fresh shell, merged with man
;; about_automatic_variables
(defvar pwsh-builtin-variables-regexp
  (regexp-opt
   '("$"                              "?"
     "^"                              "_"
     "args"                           "ConsoleFileName"
     "Error"                          "Event"
     "EventArgs"
     "EventSubscriber"                "ExecutionContext"
     "false"                          "Foreach"
     "HOME"                           "Host"
     "input"                          "lsCoreCLR"
     "lsLinux"                        "lsMacOS"
     "lsWindows"                      "LASTEXITCODE"
     "Matches"                        "MyInvocation"
     "NestedPromptLevel"              "null"
     "PID"                            "PROFILE"
     "PSBoundParameters"              "PSCmdlet"
     "PSCommandPath"
     "PSCulture"                      "PSDebugContext"
     "PSHOME"                         "PSITEM"
     "PSScriptRoot"                   "PSSenderInfo"
     "PSUICulture"                    "PSVersionTable"
     "PWD"                            "ReportErrorShowExceptionClass"
     "ReportErrorShowInnerException"  "ReportErrorShowSource"
     "ReportErrorShowStackTrace"      "Sender"
     "ShellId"                        "SourceArgs"
     "SourceEventArgs"                "StackTrace"
     "this"                           "true"                           ) t)
  "The names of the built-in PowerShell variables.
They are highlighted differently from the other variables.")

(defvar pwsh-config-variables-regexp
  (regexp-opt
   '("ConfirmPreference"           "DebugPreference"
     "ErrorActionPreference"       "ErrorView"
     "FormatEnumerationLimit"      "InformationPreference"
     "LogCommandHealthEvent"
     "LogCommandLifecycleEvent"    "LogEngineHealthEvent"
     "LogEngineLifecycleEvent"     "LogProviderHealthEvent"
     "LogProviderLifecycleEvent"   "MaximumAliasCount"
     "MaximumDriveCount"           "MaximumErrorCount"
     "MaximumFunctionCount"        "MaximumHistoryCount"
     "MaximumVariableCount"        "OFS"
     "OutputEncoding"              "ProgressPreference"
     "PSDefaultParameterValues"    "PSEmailServer"
     "PSModuleAutoLoadingPreference" "PSSessionApplicationName"
     "PSSessionConfigurationName"  "PSSessionOption"
     "VerbosePreference"           "WarningPreference"
     "WhatIfPreference"            ) t)
  "Names of variables that configure powershell features.")

(defun pwsh-find-syntactic-comments (limit)
  "Find PowerShell comment begin and comment end characters.
Returns match 1 and match 2 for <# #> comment sequences respectively.
Returns match 3 and optionally match 4 for #/eol comments.
Match 4 is returned only if eol is found before LIMIT"
  (when (search-forward "#" limit t)
    (cond
     ((looking-back "<#" nil)
      (set-match-data (list (match-beginning 0) (1+ (match-beginning 0))
                            (match-beginning 0) (1+ (match-beginning 0)))))
     ((looking-at ">")
      (set-match-data (list (match-beginning 0) (match-end 0)
                            nil nil
                            (match-beginning 0) (match-end 0)))
      (forward-char))
     (t
      (let ((start (point)))
        (if (search-forward "\n" limit t)
            (set-match-data (list (1- start) (match-end 0)
                                  nil nil nil nil
                                  (1- start) start
                                  (match-beginning 0) (match-end 0)))
          (set-match-data (list start (match-end 0)
                                nil nil nil nil
                                (1- start) start))))))
    t))

(defun pwsh-find-syntactic-quotes (limit)
  "Find PowerShell hear string begin and end sequences upto LIMIT.
Returns match 1 and match 2 for @' '@ sequences respectively.
Returns match 3 and match 4 for @\" \"@ sequences respectively."
  (when (search-forward "@" limit t)
    (cond
     ((looking-at "'$")
      (set-match-data (list (match-beginning 0) (1+ (match-beginning 0))
                            (match-beginning 0) (1+ (match-beginning 0))))
      (forward-char))
     ((looking-back "^'@" nil)
      (set-match-data (list (1- (match-end 0)) (match-end 0)
                            nil nil
                            (1- (match-end 0)) (match-end 0))))
     ((looking-at "\"$")
      (set-match-data (list (match-beginning 0) (1+ (match-beginning 0))
                            nil nil
                            nil nil
                            (match-beginning 0) (1+ (match-beginning 0))))
      (forward-char))
     ((looking-back "^\"@" nil)
      (set-match-data (list (1- (match-end 0)) (match-end 0)
                            nil nil
                            nil nil
                            nil nil
                            (1- (match-end 0)) (match-end 0)))))
    t))

(defvar pwsh-font-lock-syntactic-keywords
  `((pwsh-find-syntactic-comments (1 "!" t t) (2 "!" t t)
                                        (3 "<" t t) (4 ">" t t))
    (pwsh-find-syntactic-quotes (1 "|" t t) (2 "|" t t)
                                      (3 "|" t t) (4 "|" t t)))
  "A list of regexp's or functions.
Used to add `syntax-table' properties to
characters that can't be set by the `syntax-table' alone.")

(defvar pwsh-font-lock-keywords-1
  `( ;; Type annotations
    (,pwsh-object-types-regexp . font-lock-type-face)
    ;; syntaxic keywords
    (,pwsh-keywords . font-lock-keyword-face)
    ;; operators
    (,pwsh-operators . font-lock-builtin-face)
    ;; the REQUIRES mark
    ("^#\\(REQUIRES\\)" 1 font-lock-warning-face t))
  "Keywords for the first level of font-locking in PowerShell mode.")

(defvar pwsh-font-lock-keywords-2
  (append
   pwsh-font-lock-keywords-1
   `( ;; Built-in variables
     (,(concat "\\$\\(" pwsh-builtin-variables-regexp "\\)\\>")
      0 font-lock-builtin-face t)
     (,(concat "\\$\\(" pwsh-config-variables-regexp "\\)\\>")
      0 font-lock-builtin-face t)))
  "Keywords for the second level of font-locking in PowerShell mode.")

(defvar pwsh-font-lock-keywords-3
  (append
   pwsh-font-lock-keywords-2
   `( ;; user variables
     (,pwsh-variables-regexp
      (0 font-lock-variable-name-face)
      (1 (cons font-lock-type-face '(underline)) t t)
      (2 (cons font-lock-type-face '(underline)) t t))
     ;; function argument names
     (,pwsh-function-switch-names-regexp
      (0 font-lock-constant-face)
      (1 (cons font-lock-type-face '(underline)) t t)
      (2 (cons font-lock-type-face '(underline)) t t))
     ;; function names
     (,pwsh-function-names-regex
      (0 font-lock-function-name-face)
      (1 (cons font-lock-type-face '(underline)) t t))))
  "Keywords for the maximum level of font-locking in PowerShell mode.")

(defun pwsh-setup-font-lock ()
  "Set up the buffer local value for `font-lock-defaults'."
  ;; I use font-lock-syntactic-keywords to set some properties and I
  ;; don't want them ignored.
  (set (make-local-variable 'parse-sexp-lookup-properties) t)
  ;; This is where all the font-lock stuff actually gets set up.  Once
  ;; font-lock-defaults has its value, setting font-lock-mode true should
  ;; cause all your syntax highlighting dreams to come true.
  (setq font-lock-defaults
        ;; The first value is all the keyword expressions.
        '((pwsh-font-lock-keywords-1
           pwsh-font-lock-keywords-2
           pwsh-font-lock-keywords-3)
          ;; keywords-only means no strings or comments get fontified
          nil
          ;; case-fold (t ignores case)
          t
          ;; syntax-alist nothing special here
          nil
          ;; syntax-begin - no function defined to move outside syntactic block
          nil
          ;; font-lock-syntactic-keywords
          ;; takes (matcher (match syntax override lexmatch) ...)...
          (font-lock-syntactic-keywords
           . pwsh-font-lock-syntactic-keywords))))

(defvar pwsh-mode-syntax-table
  (let ((pwsh-mode-syntax-table (make-syntax-table)))
    (modify-syntax-entry ?$  "_"  pwsh-mode-syntax-table)
    (modify-syntax-entry ?:  "_"  pwsh-mode-syntax-table)
    (modify-syntax-entry ?-  "_"  pwsh-mode-syntax-table)
    (modify-syntax-entry ?^  "_"  pwsh-mode-syntax-table)
    (modify-syntax-entry ?\\ "_"  pwsh-mode-syntax-table)
    (modify-syntax-entry ?\{ "(}" pwsh-mode-syntax-table)
    (modify-syntax-entry ?\} "){" pwsh-mode-syntax-table)
    (modify-syntax-entry ?\[ "(]" pwsh-mode-syntax-table)
    (modify-syntax-entry ?\] ")[" pwsh-mode-syntax-table)
    (modify-syntax-entry ?\( "()" pwsh-mode-syntax-table)
    (modify-syntax-entry ?\) ")(" pwsh-mode-syntax-table)
    (modify-syntax-entry ?` "\\"  pwsh-mode-syntax-table)
    (modify-syntax-entry ?_  "w"  pwsh-mode-syntax-table)
    (modify-syntax-entry ?=  "."  pwsh-mode-syntax-table)
    (modify-syntax-entry ?|  "."  pwsh-mode-syntax-table)
    (modify-syntax-entry ?+  "."  pwsh-mode-syntax-table)
    (modify-syntax-entry ?*  "."  pwsh-mode-syntax-table)
    (modify-syntax-entry ?/  "."  pwsh-mode-syntax-table)
    (modify-syntax-entry ?' "\""  pwsh-mode-syntax-table)
    (modify-syntax-entry ?#  "<"  pwsh-mode-syntax-table)
    pwsh-mode-syntax-table)
  "Syntax for PowerShell major mode.")

(defvar pwsh-mode-map
  (let ((pwsh-mode-map (make-keymap)))
    (define-key pwsh-mode-map (kbd "M-\"")
      'pwsh-doublequote-selection)
    (define-key pwsh-mode-map (kbd "M-'") 'pwsh-quote-selection)
    (define-key pwsh-mode-map (kbd "C-'") 'pwsh-unquote-selection)
    (define-key pwsh-mode-map (kbd "C-\"") 'pwsh-unquote-selection)
    (define-key pwsh-mode-map (kbd "M-`") 'pwsh-escape-selection)
    (define-key pwsh-mode-map (kbd "C-$")
      'pwsh-dollarparen-selection)
    pwsh-mode-map)
  "Keymap for PS major mode.")

(defun pwsh-setup-menu ()
  "Add a menu of PowerShell specific functions to the menu bar."
  (define-key (current-local-map) [menu-bar pwsh-menu]
    (cons "PowerShell" (make-sparse-keymap "PWSH")))
  (define-key (current-local-map) [menu-bar pwsh-menu doublequote]
    '(menu-item "DoubleQuote Selection" pwsh-doublequote-selection
                :key-sequence(kbd "M-\"")
                :help
                "DoubleQuotes the selection escaping embedded double quotes"))
  (define-key (current-local-map) [menu-bar pwsh-menu quote]
    '(menu-item "SingleQuote Selection" pwsh-quote-selection
                :key-sequence (kbd "M-'")
                :help
                "SingleQuotes the selection escaping embedded single quotes"))
  (define-key (current-local-map) [menu-bar pwsh-menu unquote]
    '(menu-item "UnQuote Selection" pwsh-unquote-selection
                :key-sequence (kbd "C-'")
                :help "Un-Quotes the selection un-escaping any escaped quotes"))
  (define-key (current-local-map) [menu-bar pwsh-menu escape]
    '(menu-item "Escape Selection" pwsh-escape-selection
                :key-sequence (kbd "M-`")
                :help (concat "Escapes variables in the selection"
                              " and extends existing escapes.")))
  (define-key (current-local-map) [menu-bar pwsh-menu dollarparen]
    '(menu-item "DollarParen Selection" pwsh-dollarparen-selection
                :key-sequence (kbd "C-$")
                :help "Wraps the selection in $()")))


(provide 'pwsh-core)



;;; pwsh-core.el ends here
