;;; pwsh.el --- PowerShell scripting support -*- lexical-binding: t; -*-


;; Copyright (C) 2009-2010 Frédéric Perrin
;; Copyright (C) 2012 Richard Bielawski rbielaws-at-i1-dot-net
;;               http://www.emacswiki.org/emacs/Rick_Bielawski
;; Copyright (C) 2012-2022 Joe Schafer
;; Copyright (C) 2022 Maciej Barć


;; This file is part of emacs-pwsh - PowerShell support for Emacs.
;; Copyright (c) 2022, Maciej Barć <xgqt@riseup.net>
;; Licensed under the GNU GPL v3 License
;; SPDX-License-Identifier: GPL-3.0-or-later

;; emacs-pwsh is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; emacs-pwsh is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with emacs-pwsh.  If not, see <https://www.gnu.org/licenses/>.


;; Authors: Maciej Barć <xgqt@riseup.net>
;; Created: 21 Oct 2022
;; Version: 0.0.0
;; Keywords: languages
;; Homepage: https://gitlab.com/xgqt/emacs-pwsh/
;; Package-Requires: ((emacs "24.3"))
;; SPDX-License-Identifier: GPL-3.0-or-later



;;; Commentary:


;; PowerShell scripting support.

;; This is a fork of https://github.com/jschaf/powershell.el
;; aiming to have better integration with new PowerShell core.

;; powershell.el was a combination of
;; - powershell.el by Dino Chiesa <dpchiesa@hotmail.com>
;; - powershell-mode.el by Frédéric Perrin and Richard Bielawski.
;; Joe Schafer combined the work into a single file.



;;; Code:


(require 'pwsh-core)
(require 'pwsh-eldoc)
(require 'pwsh-imenu)


;;;###autoload
(define-derived-mode pwsh-mode prog-mode "PWSH"
  "Major mode for editing PowerShell scripts.

\\{pwsh-mode-map}
Entry to this mode calls the value of `pwsh-mode-hook' if
that value is non-nil."
  (pwsh-setup-font-lock)
  (setq-local indent-line-function 'pwsh-indent-line)
  (setq-local comment-start "#")
  (setq-local comment-start-skip "#+\\s*")
  (setq-local parse-sexp-ignore-comments t)
  ;; Support electric-pair-mode
  (setq-local electric-indent-chars (append "{}():;," electric-indent-chars))
  (pwsh-setup-imenu)
  (pwsh-setup-menu)
  (pwsh-setup-eldoc))


(add-hook 'pwsh-mode-hook #'imenu-add-menubar-index)

;;;###autoload
(add-to-list 'auto-mode-alist '("\\.ps[dm]?1\\'" . pwsh-mode))


(provide 'pwsh)



;;; pwsh.el ends here
