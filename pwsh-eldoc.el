;;; pwsh-eldoc.el --- PowerShell scripting support -*- lexical-binding: t; -*-


;; This file is part of emacs-pwsh - PowerShell support for Emacs.
;; Copyright (c) 2022, Maciej Barć <xgqt@riseup.net>
;; Licensed under the GNU GPL v3 License
;; SPDX-License-Identifier: GPL-3.0-or-later

;; emacs-pwsh is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; emacs-pwsh is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with emacs-pwsh.  If not, see <https://www.gnu.org/licenses/>.



;;; Commentary:


;;; PWSH Eldoc support.



;;; Code:


(defcustom pwsh-eldoc-def-files nil
  "List of files containing function help strings used by function `eldoc-mode'.
These are the strings function `eldoc-mode' displays as help for
functions near point.  The format of the file must be exactly as
follows or who knows what happens.

   (set (intern \"<fcn-name1>\" powershell-eldoc-obarray) \"<helper string1>\")
   (set (intern \"<fcn-name2>\" powershell-eldoc-obarray) \"<helper string2>\")
...

Where <fcn-name> is the name of the function to which <helper string> applies.
      <helper-string> is the string to display when point is near <fcn-name>."
  :type '(repeat string)
  :group 'pwsh)


(defvar pwsh-eldoc-obarray ()
  "Array for file entries by the function `eldoc'.
`pwsh-eldoc-def-files' entries are added into this array.")


(defun pwsh-eldoc-function ()
  "Return a documentation string appropriate for the current context or nil."
  (let ((word (thing-at-point 'symbol)))
    (if word
        (eval (intern-soft word pwsh-eldoc-obarray)))))

(defun pwsh-setup-eldoc ()
  "Load the function documentation for use with eldoc."
  (when (not (null pwsh-eldoc-def-files))
    (set (make-local-variable 'eldoc-documentation-function)
         'pwsh-eldoc-function)
    (unless (vectorp pwsh-eldoc-obarray)
      (setq pwsh-eldoc-obarray (make-vector 41 0))
      (condition-case var (mapc 'load pwsh-eldoc-def-files)
        (error (message "*** pwsh-setup-eldoc ERROR *** %s" var))))))


(provide 'pwsh-eldoc)



;;; pwsh-eldoc.el ends here
